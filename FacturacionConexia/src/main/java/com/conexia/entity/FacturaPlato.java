package com.conexia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detalle_factura_plato")
public class FacturaPlato extends ModelPadre implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "cantidad")
    private Integer cantidad;

    @Column(name = "importe")
    private Double importe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_plato")
    private Plato plato;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cocinero")
    private Usuario cocinero;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public Plato getPlato() {
        return plato;
    }

    public void setPlato(Plato plato) {
        this.plato = plato;
    }

    public Usuario getCocinero() {
        return cocinero;
    }

    public void setCocinero(Usuario cocinero) {
        this.cocinero = cocinero;
    }

    public void calcularImporte() {
        importe = plato.getValor()*cantidad;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexia.entity.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author INTEL
 */
public class UsuarioDto {

    private static final long serialVersionUID = 1L;

    private String identificacion;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private BigInteger numFacturas;
    private BigDecimal totalCompras;

    public UsuarioDto(Object[] object) {
        this.identificacion = (String) object[0];
        this.nombre = (String) object[1];
        this.totalCompras = (BigDecimal) object[2];
        this.numFacturas = (BigInteger) object[3];
        
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public BigInteger getNumFacturas() {
        return numFacturas;
    }

    public void setNumFacturas(BigInteger numFacturas) {
        this.numFacturas = numFacturas;
    }

    public BigDecimal getTotalCompras() {
        return totalCompras;
    }

    public void setTotalCompras(BigDecimal totalCompras) {
        this.totalCompras = totalCompras;
    }

}

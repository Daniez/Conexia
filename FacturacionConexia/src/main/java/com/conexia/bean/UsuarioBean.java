package com.conexia.bean;

import com.conexia.entity.Usuario;
import com.conexia.service.usuario.IUsuarioService;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

@Named
@RequestScoped
public class UsuarioBean {
	
	@Autowired(required = true)
	private IUsuarioService usuarioService;

	@Getter @Setter 
	private List<Usuario> usuarios;

	@PostConstruct
	public void init() {
		usuarios = usuarioService.buscarTodo();
	}
	
}

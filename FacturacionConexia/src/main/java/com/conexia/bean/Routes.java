package com.conexia.bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Routes implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:/Factura/factura.xhtml");
		registry.addViewController("/factura").setViewName("forward:/Factura/factura.xhtml");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}
}
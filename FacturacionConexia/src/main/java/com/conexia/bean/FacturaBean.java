package com.conexia.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.conexia.entity.Factura;
import com.conexia.entity.FacturaPlato;
import com.conexia.entity.Mesa;
import com.conexia.entity.Plato;
import com.conexia.entity.Usuario;
import com.conexia.entity.dto.UsuarioDto;
import com.conexia.service.factura.IFacturaService;
import com.conexia.service.usuario.IUsuarioService;
import com.conexia.util.Cargo;
import com.conexia.util.Message;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.faces.view.ViewScoped;

import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class FacturaBean implements Serializable {

    @Autowired(required = true)
    private IFacturaService facturaService;

    @Autowired(required = true)
    private IUsuarioService usuarioService;

    @Getter
    @Setter
    private Usuario cliente;

    @Getter
    @Setter
    private List<Usuario> clientes;
    
    @Getter
    @Setter
    private List<UsuarioDto> clientesPremium;
    
    @Getter
    @Setter
    private List<UsuarioDto> camarerosReporte;
    
    @Getter
    @Setter
    private List<Usuario> cocineros;

    @Getter
    @Setter
    private List<Usuario> camareros;

    @Getter
    @Setter
    private FacturaPlato facturaPlato;

    @Getter
    @Setter
    private Factura factura;
    
    @Getter
    @Setter
    private List<Factura> facturas ;

    @Getter
    @Setter
    private List<Plato> platos;

    @Getter
    @Setter
    private List<Mesa> mesas;

    @Getter
    @Setter
    private List<FacturaPlato> itemsFactura;

    @Getter
    @Setter
    private int cantidad;
    private Message mensaje;

    @Getter
    @Setter
    private BigDecimal totalFactura;
    
    
    @Getter
    @Setter
    private String fechaReporte;
    
    @Getter
    @Setter
    private boolean verFactura, crearFactura, listarFacturas, verPremium, verReporte;

    @PostConstruct
    void init() {
        mensaje = new Message();
        verListarPanel();
    }

    public void agregarPlato() {
        if (facturaPlato.getPlato() != null) {

            facturaPlato.calcularImporte();
            itemsFactura.add(facturaPlato);
            mensaje.info("Agregó el plato " + facturaPlato.getPlato().getNombre());
            totalFactura();
        } else {
            mensaje.error("Seleccione un plato");
        }
        platos.remove(facturaPlato.getPlato());
        facturaPlato = new FacturaPlato();

    }

    public void cambiarCantidad(FacturaPlato item) {
        item.calcularImporte();
        totalFactura();
    }

    private void totalFactura() {
        totalFactura = new BigDecimal(0);
        for (FacturaPlato facturaPlato : itemsFactura) {
            totalFactura = totalFactura.add(new BigDecimal(facturaPlato.getImporte()));
        }
    }

    private void buscarUsuarios() {
        clientes = usuarioService.buscarporCargo(Cargo.CLIENTE.toString());
        camareros = usuarioService.buscarporCargo(Cargo.CAMARERO.toString());
        cocineros = usuarioService.buscarporCargo(Cargo.COCINERO.toString());
    }

    public void buscarCliente() {

        Usuario clienteTemp = usuarioService.buscarPorIdentificacion(cliente.getIdentificacion());
        if (clienteTemp != null) {
            cliente = clienteTemp;
        }
    }

    public void guardarFactra() {
        try {
            if(itemsFactura.isEmpty()){
                 mensaje.error("Debe agregar almenos un producto");
                return;
            }
            cliente.setCargo(Cargo.CLIENTE);
            cliente = usuarioService.guardar(cliente);
            factura.setTotal(totalFactura);
            factura.setCliente(cliente);
            factura.setPlatos(itemsFactura);
            facturaService.guardar(factura);
            mensaje.info("Registo Exitoso");
            verListarPanel();
        } catch (Exception e) {
            mensaje.error("Error al guardar factura");
        }
    }

    public void verAgregarPanel() {
        esconderPaneles();
        cliente = new Usuario();
        totalFactura = new BigDecimal(0);
        factura = new Factura();
        buscarUsuarios();
        facturaPlato = new FacturaPlato();
        itemsFactura = new ArrayList<>();
        platos = facturaService.listaPlatos();
        mesas = facturaService.listarMesas();
        crearFactura = true;
    }

    public void verListarPanel() {
        esconderPaneles();
        facturas = facturaService.buscarTodo();
        listarFacturas = true;
    }
    
    
    
    public void verFacturaPanel(Factura fac) {
        esconderPaneles();
        Factura factura = facturaService.buscarPorId(fac.getId());
        cliente =new Usuario();
        cliente = factura.getCliente();
        totalFactura =  factura.getTotal();
        this.factura = factura;
        buscarUsuarios();
        platos = facturaService.listaPlatos();
        mesas = facturaService.listarMesas();
        itemsFactura = new ArrayList<>();
        itemsFactura = factura.getPlatos();
        verFactura = true;
    }
    
    public void verPanelPremium() {
        esconderPaneles();
        clientesPremium = usuarioService.clientesPremium();
        verPremium = true;
    }
    
    public void esconderPaneles() {
        verFactura = false;
        crearFactura = false;
        listarFacturas = false;
        verPremium = false;
        verReporte = false;
    }
    
    public void verCamarerosReprote() {
        esconderPaneles();
        camarerosReporte = new ArrayList<>();
        verReporte = true;
    }
    public void generaReporte() {
        try {
             camarerosReporte = usuarioService.camarerosReporte(fechaReporte);
        } catch (Exception e) {
            camarerosReporte = new ArrayList<>();
            mensaje.info("Verifique la fehca ingresada");
        }
       
    }
    
}

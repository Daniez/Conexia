package com.conexia.service.factura;

import java.util.List;

import com.conexia.entity.Factura;
import com.conexia.entity.Mesa;
import com.conexia.entity.Plato;

public interface IFacturaService {

    public List<Factura> buscarTodo();

    public Factura buscarPorId(Long id);

    public Factura guardar(Factura factura) throws Exception;

    public List<Plato> listaPlatos();

    public List<Mesa> listarMesas();

}

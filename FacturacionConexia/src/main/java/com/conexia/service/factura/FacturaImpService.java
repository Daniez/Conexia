package com.conexia.service.factura;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.dao.IPlatoDao;

import com.conexia.dao.IFacturaDao;
import com.conexia.dao.IFacturaPlatoDao;
import com.conexia.dao.IMesaDao;
import com.conexia.entity.Factura;
import com.conexia.entity.Mesa;
import com.conexia.entity.Plato;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;

@Service
public class FacturaImpService implements IFacturaService {

    @Autowired
    private IPlatoDao platoDao;

    @Autowired
    private IMesaDao mesaDao;

    @Autowired
    private IFacturaDao facturaDao;

    @Autowired
    private IFacturaPlatoDao facturaPlatoDao;

    @Override
    public List<Factura> buscarTodo() {
        // TODO Auto-generated method stub
        return (List<Factura>) facturaDao.findAll(new Sort(Sort.Direction.DESC, "Id"));
    }


    @Override
    @Transactional(readOnly = true)
    public Factura buscarPorId(Long id) {
        // TODO Auto-generated method stub
        return facturaDao.findById(id).orElse(null);
    }

    @Override
    public Factura guardar(Factura factura) throws Exception {
        // TODO Auto-generated method stub

        return facturaDao.save(factura);
    }

    @Override
    public List<Plato> listaPlatos() {
        // TODO Auto-generated method stub
        return (List<Plato>) platoDao.findAll();
    }

    @Override
    public List<Mesa> listarMesas() {
        return (List<Mesa>) mesaDao.findAll();
    }
    
    
}

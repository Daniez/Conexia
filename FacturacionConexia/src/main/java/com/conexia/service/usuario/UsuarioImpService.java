package com.conexia.service.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conexia.dao.IUsuarioDao;
import com.conexia.entity.Usuario;
import com.conexia.entity.dto.UsuarioDto;
import java.util.ArrayList;

@Service
public class UsuarioImpService implements IUsuarioService {

    @Autowired
    private IUsuarioDao usuarioDao;

    @Override
    public List<Usuario> buscarTodo() {
        // TODO Auto-generated method stub
        return (List<Usuario>) usuarioDao.findAll();
    }

    @Override
    public Usuario buscarPorId(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Usuario guardar(Usuario usuario) throws Exception {
        // TODO Auto-generated method stub

        return usuarioDao.save(usuario);

    }

    @Override
    public List<Usuario> buscarporCargo(String cargo) {
        return (List<Usuario>) usuarioDao.buscarCargo(cargo); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario buscarPorIdentificacion(String identificacion) {
        return (Usuario) usuarioDao.buscarIdentificacion(identificacion);  //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UsuarioDto> clientesPremium() {
        List<UsuarioDto> listaUsuariosPremium = new ArrayList<>();
        for (Object[] object : (List<Object[]>) usuarioDao.clientePremium()) {
            UsuarioDto u = new UsuarioDto(object);
            listaUsuariosPremium.add(u);
        }
        return listaUsuariosPremium ;
    }
    
    @Override
    public List<UsuarioDto> camarerosReporte(String fecha) throws Exception{
        List<UsuarioDto> listaUsuariosPremium = new ArrayList<>();
        for (Object[] object : (List<Object[]>) usuarioDao.reporteCamareros(fecha)) {
            UsuarioDto u = new UsuarioDto(object);
            listaUsuariosPremium.add(u);
        }
        return listaUsuariosPremium ;
    }
}

package com.conexia.service.usuario;

import java.util.List;

import com.conexia.entity.Usuario;
import com.conexia.entity.dto.UsuarioDto;

public interface IUsuarioService {

    public List<Usuario> buscarTodo();

    public Usuario buscarPorId(Long id);

    public Usuario guardar(Usuario usuario) throws Exception;

    public List<Usuario> buscarporCargo(String id);

    public Usuario buscarPorIdentificacion(String identificacion);
    
    public List<UsuarioDto> clientesPremium();
    
    public List<UsuarioDto> camarerosReporte(String fecha) throws Exception;
}

package com.conexia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturacionConexiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacturacionConexiaApplication.class, args);
	}

}

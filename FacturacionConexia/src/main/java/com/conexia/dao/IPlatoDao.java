package com.conexia.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conexia.entity.Plato;

public interface IPlatoDao extends JpaRepository<Plato, Long>{

}

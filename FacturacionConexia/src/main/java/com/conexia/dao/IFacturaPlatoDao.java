package com.conexia.dao;



import com.conexia.entity.Factura;
import com.conexia.entity.FacturaPlato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFacturaPlatoDao extends JpaRepository<FacturaPlato, Long>{

}

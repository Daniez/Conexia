package com.conexia.dao;



import com.conexia.entity.Factura;
import com.conexia.entity.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IFacturaDao extends JpaRepository<Factura, Long>{
    
}

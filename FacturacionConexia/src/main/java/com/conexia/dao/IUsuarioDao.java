package com.conexia.dao;

import com.conexia.entity.Factura;
import org.springframework.data.jpa.repository.JpaRepository;

import com.conexia.entity.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

;

public interface IUsuarioDao extends JpaRepository<Usuario, Long> {

    @Query("SELECT u FROM Usuario u WHERE LOWER(u.cargo) = LOWER(:cargo)")
    public List<Usuario> buscarCargo(@Param("cargo") String cargo);

    @Query("SELECT u FROM Usuario u WHERE u.identificacion = :identificacion")
    public Usuario buscarIdentificacion(@Param("identificacion") String identificacion);

    @Query(name = "clientesPremium",
            value = "select u.identificacion ,concat(u.nombre , u.primer_apellido ), sum(f.total) AS total, count(f.id)  numFacturas from  facturas f "
            + " left join usuarios u on f.id_cliente = u.id where total >= 100000 "
            + " group by u.id",
            nativeQuery = true)
    public List<Object[]> clientePremium();

    @Query(name = "reporteCamareros",
            value = "select u.identificacion, concat(u.nombre , ' ',u.primer_apellido ) as fullName, "
            + " sum(coalesce(f.total, 0)) as total, count(f.id) as compras from  usuarios u "
            + " left join ("
            + "	select ff.total, ff.fecha_factura, ff.id, ff.id_camarero"
            + "    from facturas ff where  CAST(ff.fecha_factura as CHAR) LIKE  :fecha% "
            + ") as f on f.id_camarero = u.id "
            + " where u.cargo = 'CAMARERO'"
            + " group by u.id",
            nativeQuery = true)
    public List<Object[]> reporteCamareros(@Param("fecha") String fecha);
}

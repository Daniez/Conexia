/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexia.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author INTEL
 */
public class Message {
        
    public void info(String msg) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",  msg));
    }
    
    /**
     * metodo usado para mostrar mensajes de advertencia en la pantalla.
     * 
     * @param msg 
     *  mensaje que se desea mostrar.
     */
    public void warn(String msg) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", msg));

    }
    
    /**
     * metodo usado para mostrar mensajes de error en la pantalla.
     * 
     * @param msg 
     *  mensaje que se desea mostrar.
     */
    public void error(String msg) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", msg));
    }
    
    /**
     * metodo usado para mostrar mensajes de fatalidad en la pantalla.
     * 
     * @param msg 
     *  mensaje que se desea mostrar.
     */
    public void fatal(String msg) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal!", msg));
    }
}

create database conexia;
use conexia;

CREATE TABLE IF NOT EXISTS `usuarios` (
  id bigint  NOT NULL AUTO_INCREMENT,
  identificacion varchar(20) NOT NULL,
  nombre varchar(30)   NOT NULL,
  primer_apellido varchar(30)   NOT NULL,
  segundo_apellido varchar(30)    NULL,
  observaciones  varchar(500)    NULL,
  cargo   varchar(30)    NULL,
  PRIMARY KEY (id),
  UNIQUE KEY identificacion_unique (identificacion)
);

CREATE TABLE IF NOT EXISTS `mesas` (
  id bigint  NOT NULL AUTO_INCREMENT,
  num_mesa numeric(10) NOT NULL,
  ubicacion varchar(30)   NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY num_mesa_unique (num_mesa)
);


CREATE TABLE IF NOT EXISTS `facturas` (
  id bigint  NOT NULL AUTO_INCREMENT,
  id_cliente  bigint NOT NULL,
  id_camarero  bigint NOT NULL,
  id_mesa  bigint NOT NULL,
  total numeric(12),
  fecha_factura timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  KEY `fkid_factura_id_cliente` (`id_cliente`),
  KEY `fkid_factura_id_camarero` (`id_camarero`),
  KEY `fkid_factura_id_mesa` (`id_mesa`)
);

ALTER TABLE `facturas`
  ADD CONSTRAINT `fkid_factura_id_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fkid_factura_id_camarero` FOREIGN KEY (`id_camarero`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fkid_factura_id_mesa` FOREIGN KEY (`id_mesa`) REFERENCES `mesas` (`id`);
  
  
 CREATE TABLE IF NOT EXISTS `platos` (
  id bigint  NOT NULL AUTO_INCREMENT,
  codigo bigint(8) NOT NULL,
  nombre varchar(30)   NOT NULL,
  valor numeric(12),
  PRIMARY KEY (id),
  UNIQUE KEY cod_plato_unique (codigo)
);


CREATE TABLE IF NOT EXISTS `detalle_factura_plato` (
  id bigint  NOT NULL AUTO_INCREMENT,
  id_factura  bigint ,
  id_plato  bigint NOT NULL,
  id_cocinero  bigint NOT NULL,
  cantidad int(12),
  importe numeric(12),
  PRIMARY KEY (id),
  KEY `fkid_det_factura_id_cocinero` (`id_cocinero`),
  KEY `fkid_det_factura_id_plato` (`id_plato`),
  KEY `fkid_det_factura_id_factura` (`id_factura`)
);


ALTER TABLE `detalle_factura_plato`
  ADD CONSTRAINT `fkid_det_factura_id_cocinero` FOREIGN KEY (`id_cocinero`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fkid_det_factura_id_plato` FOREIGN KEY (`id_plato`) REFERENCES `platos` (`id`),
  ADD CONSTRAINT `fkid_det_factura_id_factura` FOREIGN KEY (`id_factura`) REFERENCES `facturas` (`id`);
  
  
  
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1234567, 'Daniel', 'Bustamante', 'Tamayo','', 'CLIENTE');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1234568, 'Sara', 'Bustamante', 'Tamayo','', 'CLIENTE');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1234569, 'Carolina', 'Bustamante', 'Tamayo','', 'CLIENTE');

Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1334567, 'Miguel', 'Perea', 'Rodriguez','', 'COCINERO');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1334568, 'Alejandro', 'Perea', 'Rodriguez','', 'COCINERO');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1334569, 'Sofia', 'Perea', 'Rodriguez','', 'COCINERO');


Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1434567, 'Juan', 'Rodriguez', 'Rodriguez','', 'CAMARERO');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1434568, 'Felipe', 'Rodriguez', 'Rodriguez','', 'CAMARERO');
Insert Into usuarios (identificacion, nombre, primer_apellido, segundo_apellido, observaciones, cargo ) values (1434569, 'Carlos', 'Rodriguez', 'Rodriguez','', 'CAMARERO');

Insert Into platos (codigo, nombre, valor ) values (1, 'Bandeja Paisa', 12000);
Insert Into platos (codigo, nombre, valor ) values (2, 'Agiaco', 10000);
Insert Into platos (codigo, nombre, valor ) values (3, 'Trucha', 10200);
Insert Into platos (codigo, nombre, valor ) values (4, 'Mojara', 10700);
Insert Into platos (codigo, nombre, valor ) values (5, 'Lomo de Cerdo', 15700);

Insert Into mesas (num_mesa, ubicacion ) values (1, 'Centro');
Insert Into mesas (num_mesa, ubicacion ) values (2, 'Centro');
Insert Into mesas (num_mesa, ubicacion ) values (3, 'Centro');
Insert Into mesas (num_mesa, ubicacion ) values (4, 'Izquierda');
Insert Into mesas (num_mesa, ubicacion ) values (5, 'Izquierda');


select * from facturas;


select * from usuarios;


select * from detalle_factura_plato;


select concat(u.nombre , '',u.primer_apellido ) as fullName, 
sum(coalesce(f.total, 0)) as total, count(f.id) as compras, u.id, u.nombre from  facturas f
left join usuarios u on f.id_cliente = u.id
where total >= 100000
group by u.id;

select concat(u.nombre , ' ',u.primer_apellido ) as fullName, 
sum(coalesce(f.total, 0)) as total, count(f.id) as compras from  usuarios u
left join (
	select ff.total, ff.fecha_factura, ff.id, ff.id_camarero
    from facturas ff where  CAST(ff.fecha_factura as CHAR) LIKE '2019-05%'
) as f on f.id_camarero = u.id
where u.cargo = 'CAMARERO'
group by u.id;
